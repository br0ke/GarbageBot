import sys

from garbageBot import GitlabServer


if __name__ == '__main__':
    gls = GitlabServer()
    docker_images_to_delete = []
    docker_registry = gls.get_registry_info()
    # can a project have more than one registry? yes it can!
    for reg in docker_registry:
        docker_images_to_delete += gls.get_image_tags(reg['tags_path'])

    if gls.purge_all:
        print('All images in the registry will be deleted')
    else:
        if gls.keep_branch_images:
            docker_images_to_delete = gls.return_list_of_non_branch_images(docker_images_to_delete)
        if 'gl_images_to_keep' in gls.settings:
            docker_images_to_delete = gls.return_list_without_kept_images(docker_images_to_delete)
        if 'gl_images_to_delete' in gls.settings:
            docker_images_to_delete = gls.return_declared_image_objects(docker_images_to_delete)
        if 'gl_delete_older_than' in gls.settings:
            docker_images_to_delete = gls.return_old_images_list(docker_images_to_delete)
        if 'gl_keep_latest_versions' in gls.settings:
            docker_images_to_delete = gls.remove_latest_images_from_list(gls.settings['gl_keep_latest_versions'],
                                                                         docker_images_to_delete)
    if len(docker_images_to_delete) == 0:
        print('No images found for deletion, check your filters!')

    for image in docker_images_to_delete:
        del_resp = gls.delete_image(image)

        if del_resp and del_resp.status_code == 204:
            print('image {0} deleted'.format(image['name']))
        elif del_resp and not del_resp.status_code == 204:
            print('something went wrong. HTTP Status {0}, HTTP Content {1}'.format(del_resp.status_code, del_resp.text))
        elif not del_resp and not gls.settings['dry_run']:
            print('not deleting image {0}.'.format(image['name']))

    print('done')
    sys.exit()
