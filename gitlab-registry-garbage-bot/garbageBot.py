import sys
import os
import re
import datetime

from operator import itemgetter

import requests
import gitlab
from lxml import html
import urllib3
import iso8601

# Disabling the certificate warning (only during runtime)
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class GitlabServer(object):
    def __init__(self):
        self.settings = self.read_settings()
        self.gl_server_base = self.settings['gl_server_base']
        self.gl_auth_token = self.settings['gl_auth_token']
        self.gl_base_project_id = self.settings['gl_base_project_id']
        self.gl_base_project_path = self.settings['gl_base_project_path']
        self.purge_all = self.settings['purge_all']
        self.keep_branch_images = self.settings['gl_keep_branch_images']
        self.gl_api = gitlab.Gitlab(self.gl_server_base, self.gl_auth_token, api_version='4', ssl_verify=False)
        self.request_headers = {
            'Private-Token': self.gl_auth_token
        }
        self.reg_resp = self.get_response_csrf_session()
        self.sess_cookie = dict(_gitlab_session=self.reg_resp.cookies['_gitlab_session'])
        self.csrf_token = self.extract_csrf_token(self.reg_resp.text)

    def get_response_csrf_session(self):
        """
        Function to get a response object to extract the session cookie and the csrf token.
        Calling /container_registry here but this could be any page really
        :return: response object
        """
        return requests.get('{0}/{1}/container_registry'.format(self.gl_server_base, self.gl_base_project_path),
                            headers=self.request_headers)

    def get_project_branches(self):
        """
        Function returns a list of branches for the project
        :return: a list of branches for the project as api object
        """
        project_object = self.gl_api.projects.get(self.gl_base_project_id)
        return project_object.branches.list(all=True)

    def get_registry_info(self):
        """
        Get the docker registry info for project
        :return: registry info as JSON
        """
        resp = requests.get('{0}/{1}/container_registry.json'.format(self.gl_server_base, self.gl_base_project_path),
                            headers=self.request_headers)
        return resp.json()

    def get_image_tags(self, tags_path):
        """
        Getting the image tags from registry as JSON
        we need to split the tags_path as we are building our own request params
        :return: all images in registry as JSON
        """
        get_params = {
            'format': 'json',
            'per_page': '1000'
        }
        return requests.get('{0}{1}'.format(self.gl_server_base, tags_path.split('?')[0]),
                            headers=self.request_headers, params=get_params).json()

    def delete_image_from_registry(self, image_path):
        """
        Deleting the image with the given path
        :return: response object
        """
        if not self.settings['dry_run']:
            self.request_headers['X-CSRF-Token'] = self.csrf_token
            return requests.delete("{0}{1}".format(self.gl_server_base, image_path),
                                   headers=self.request_headers, cookies=self.sess_cookie)

    def return_declared_image_objects(self, registry_list):
        """
        returns a list of image dictionaries whose names are defined in the config
        :param registry_list: complete list of images for the project's registry
        :return: list of image dictionaries
        """
        list_to_delete = []
        for image in registry_list:
            if image['name'] in self.settings['gl_images_to_delete']:
                list_to_delete.append(image)
        return list_to_delete

    def return_old_images_list(self, registry_list):
        """
        returns a list of images that are older than the defined amount of days in the config
        :param registry_list: complete list of images for the project's registry
        :return: list of image dictionaries
        """
        list_to_delete = []
        for image in registry_list:
            img_created = iso8601.parse_date(image['created_at'])
            delta = (datetime.datetime.now() - img_created.replace(tzinfo=None)).days
            if delta > int(self.settings['gl_delete_older_than']):
                print('image {0} is older than expiry of {1} days'.format(image['name'],
                                                                          self.settings['gl_delete_older_than']))
                list_to_delete.append(image)
        return list_to_delete

    def return_list_of_non_branch_images(self, registry_list):
        """
        returns a list of the images that do not have a branch associated
        :param registry_list: complete list of images for the project's registry
        :return: list of image dictionaries
        """
        list_to_delete = []
        images_to_keep = self.get_list_of_normalized_branches(self.get_project_branches())
        for image in registry_list:
            if image['name'] not in images_to_keep:
                list_to_delete.append(image)
        return list_to_delete

    def return_list_without_kept_images(self, registry_list):
        """
        returns a list of the images that doesn't include the images in the kept list
        :param registry_list: complete list of images for the project's registry
        :return: list of image dictionaries
        """
        for image in registry_list:
            if image['name'] in self.settings['gl_images_to_keep']:
                registry_list.remove(image)
        return registry_list

    def delete_image(self, image):
        """
        actually deletes an image from the registry
        :param image: a dictionary containing the image information
        :return: response from the gitlab server
        """
        if self.settings['dry_run']:
            print('image {0} WILL be deleted. DRY RUN!!!'.format(image['name']))
        else:
            print('deleting ' + image['name'])
            return self.delete_image_from_registry(image['destroy_path'])

    @staticmethod
    def get_list_of_normalized_branches(branches_list):
        """
        Add branches to a list
        :param branches_list: A list of branches from Gitlab API
        :return: a list of normalized branches
        """
        b_list = []
        for branch in branches_list:
            b_list.append(GitlabServer.transform_branch_ci_commit_ref_slug(branch_name=branch.attributes['name']))
        return b_list

    @staticmethod
    def remove_latest_images_from_list(number_of_images, registry_list):
        """
        this removes x number of the latest images from the list of images to be deleted
        :param number_of_images: how many of the latest images to retain
        :param registry_list: complete list of images for the project's registry
        :return: list of image dictionaries
        """
        time_sorted_list = sorted(registry_list, key=itemgetter('created_at'), reverse=True)
        shortened_list = time_sorted_list[int(number_of_images):]
        return shortened_list

    @staticmethod
    def transform_branch_ci_commit_ref_slug(branch_name):
        """
        Transfornms a Gitlab Branch name to the CI_COMMIT_REF_SLUG
        https://docs.gitlab.com/ce/ci/variables/README.html#predefined-variables-environment-variables

        :param branch_name:
        :return: ci_commit_ref_slug, a normalized version of the branch name
        """
        return re.sub(r'[^a-z0-9-]', '-', branch_name.lower())

    @staticmethod
    def extract_csrf_token(raw_html):
        """
        Function to extract the csrf token
        :param raw_html: a raw html page containing the csfr-token in a meta tag
        :return: the csfr token as string
        """
        html_object = html.fromstring(raw_html)
        token_object = html_object.xpath('/html/head/meta[@name="csrf-token"]')
        return token_object[0].attrib['content']

    @staticmethod
    def read_settings():
        """
        Function reads the settings from the environment
        :return: dict of settings
        """
        try:
            settings_dict = {
                'gl_server_base': os.environ['GL_SERVER_BASE'],
                'gl_auth_token': os.environ['GL_AUTH_TOKEN'],
                'gl_base_project_id': os.environ['GL_BASE_PROJECT_ID'],
                'gl_base_project_path': os.environ['GL_BASE_PROJECT_PATH'],
                'gl_keep_branch_images': True,
                'purge_all': False,
                'dry_run': False
            }
            try:
                if os.environ['GL_DRY_RUN'] == 'True':
                    settings_dict['dry_run'] = True
                    print('Dry Run set to true, nothing should be deleted, only what would be deleted will be shown '
                          'in the output')
            except KeyError as e:
                print('{0} not set, defaulting to false'.format(str(e)))
            try:
                if os.environ['GL_PURGE_ALL'] == 'True':
                    settings_dict['purge_all'] = True
                    print('Purge all set to true.  All images will be deleted and all other options set will be '
                          'ignored!')
            except KeyError as e:
                print('{0} not set, defaulting to false'.format(str(e)))
                try:
                    if os.environ['GL_KEEP_BRANCH_IMAGES'] == 'False':
                        settings_dict['gl_keep_branch_images'] = False
                        print('Keep branched images set to true, all images with a corresponding branch will not be '
                              'deleted')
                except KeyError as e:
                    print('{0} not set, defaulting to true. All images that correspond to a branch will be kept'
                          .format(str(e)))
                try:
                    settings_dict['gl_images_to_delete'] = os.environ['GL_IMAGES_TO_DELETE'].split(',')
                    print('Images specified for deletion, the images specified will be deleted')
                except KeyError as e:
                    print('{0} not set, using existing branches to determine which images are deleted.'.format(str(e)))
                try:
                    settings_dict['gl_images_to_keep'] = os.environ['GL_IMAGES_TO_KEEP'].split(',')
                    print('Images specified for retention, the images specified will not be deleted')
                except KeyError as e:
                    print('{0} not set, no images are specified for retention.'.format(str(e)))
                try:
                    settings_dict['gl_delete_older_than'] = os.environ['GL_DELETE_OLDER_THAN']
                    print('Older than setting has been set, all images older than {0} days will be deleted.'
                          .format(os.environ['GL_DELETE_OLDER_THAN']))
                except KeyError as e:
                    print('{0} not set, no expiration date will be used.'.format(str(e)))
                try:
                    settings_dict['gl_keep_latest_versions'] = os.environ['GL_KEEP_LATEST_VERSIONS']
                    print('Keep latest versions has been set.  The last {0} versions of your image will not be deleted'
                          .format(os.environ['GL_KEEP_LATEST_VERSIONS']))
                except KeyError as e:
                    print('{0} not set, latest versions will not be kept.'.format(str(e)))

        except KeyError as e:
            print('Environment Variable {0} not found. Exiting.'.format(str(e)))
            sys.exit(1)
        return settings_dict
