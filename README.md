# GarbageBot
## General Notes
This project provides functionality that is not available in GitLab itself. It will clean your GitLab registry of unneeded docker images.  
Please note that this is only de-referencing images and not really deleting them. The actual deletion is done by
serverside tasks ([gitlab-registry-cleanup](https://github.com/sciapp/gitlab-registry-cleanup) and the [registry-garbage-collect](https://docs.gitlab.com/omnibus/maintenance/#container-registry-garbage-collection)) that have to be scheduled via cron.  
### Status
[![pipeline status](https://gitlab.com/paessler-labs/GarbageBot/badges/develop/pipeline.svg)](https://gitlab.com/paessler-labs/GarbageBot/commits/develop)  


[![coverage report](https://gitlab.com/paessler-labs/GarbageBot/badges/develop/coverage.svg)](https://gitlab.com/paessler-labs/GarbageBot/commits/develop)


## Installation  
- clone the repository  
  
### Local  
- create a virtual env  
- activate the venv  
- install requirements  
  
### Container  
- run docker build  

or

- use docker pull paesslerlabs/garbagebot

## Usage
The GarbageBot is configured via environment variables. The following variables are available.

- GL_SERVER_BASE -> GitLab server URL
- GL_AUTH_TOKEN -> Your auth token, the user needs access to the registry
- GL_BASE_PROJECT_PATH -> Your project path
- GL_BASE_PROJECT_ID -> The ID of the project
- [GL_PURGE_ALL] -> Set True or False; if not set, it will default to false (optional) *see warnings
- [GL_KEEP_BRANCH_IMAGES] -> Set True or False; if not set, it will default to True (optional)
- [GL_IMAGES_TO_DELETE] -> Pass a comma separated list of image names; if not set, default operation mode will be assumed (optional)
- [GL_IMAGES_TO_KEEP] -> Pass a comma separated list of image names that should be kept; if not set, default operation mode will be assumed (optional)
- [GL_DELETE_OLDER_THAN] -> Number of days after which images are deleted (optional)
- [GL_KEEP_LATEST_VERSIONS] -> Set to the number of latest versions you want to keep (optional)
- [GL_DRY_RUN] -> Set True or False; if not set, it will default to false (optional)

```
- WARNINGS!!

1. If no optional setting is set, all images without a branch will be deleted.

2. The config option GL_PURGE_ALL WILL ALWAYS DELETE ALL IMAGES! All other optional settings will be ignored!

3. Use GL_DRY_RUN when testing!

```
---

The config options GL_KEEP_BRANCH_IMAGES , GL_IMAGES_TO_DELETE, GL_DELETE_OLDER_THAN, GL_KEEP_LATEST_VERSIONS can be used together and are evaluated in this order. Please make sure to test with
the dry-run option to see what the results will be.

## Run  
### Container  
```bash
docker run -it -e GL_SERVER_BASE=GITLAB_URL -e GL_AUTH_TOKEN=ACCESS_TOKEN -e GL_BASE_PROJECT_PATH=PATH_TO_PROJECT -e GL_BASE_PROJECT_ID=PROJECT_ID paesslerlabs/garbagebot
```
### Local  
- set Environment Variables:   
    ```bash
    export GL_SERVER_BASE=GITLAB_URL
    export GL_AUTH_TOKEN=ACCESS_TOKEN
    export GL_BASE_PROJECT_PATH=PATH_TO_PROJECT
    export GL_BASE_PROJECT_ID=PROJECT_ID 
    ```

- call `python run.py` or write your own code to run.

### GitLab CI
- add new job to your `.gitlab-ci.yml` file, for example:
```yaml
cleanup_registry:
  image: paesslerlabs/garbagebot
  variables:
    GL_SERVER_BASE: https://gitlab.com
    GL_AUTH_TOKEN: ACCESS_TOKEN
    GL_BASE_PROJECT_PATH: $CI_PROJECT_PATH
    GL_BASE_PROJECT_ID: $CI_PROJECT_ID
    GL_KEEP_BRANCH_IMAGES: True
```

## ToDos
- more unit tests
- possibly more documentation

## Known issues
- sometimes (not reproducible) the Gitlab Registry API throws an HTTP Status 400, the image is deleted anyways. No error message in the logs except the HTTP Status 400.

## Contributors
Konstantin Wolff, Paessler AG  
Greg Campion, Paessler AG  
Enrico Weigler, Paessler AG  